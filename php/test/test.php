<?php

require_once('../fcgi.php');

$target_script = __DIR__.'/dummy.php';
$body = "あいうえおあいうえおあいうえおあいうえおあいうえおあいうえおあいうえおあいうえおあいうえおあいうえおあいうえおあいうえおあいうえおあいうえお\n";
foreach(range(1, 10) as $i) {
    $body .= $body;
}
$body = 'START'.$body;
$body .= 'END';
$client = new FCGIClient();
list($stdout, $stderr) = $client->send([
        'QUERY_STRING' => '?param=hello',
        'REQUEST_METHOD' => 'GET',
        'SCRIPT_FILENAME' => $target_script,
        'CONTENT_TYPE' => 'application/x-www-form-urlencoded',
        'CONTENT_LENGTH' => strlen($body),
    ],
    $body
);

echo($stdout);
echo(PHP_EOL);


