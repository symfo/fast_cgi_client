<?php

define('VERSION', '0.0.2');

class FCGIClient {

    //
    // 8. Types and Constants
    //
    const FCGI_VERSION_1 = 1;

    const FCGI_BEGIN_REQUEST = 1;
    const FCGI_ABORT_REQUEST = 2;
    const FCGI_END_REQUEST = 3;
    const FCGI_PARAMS = 4;
    const FCGI_STDIN = 5;
    const FCGI_STDOUT = 6;
    const FCGI_STDERR = 7;
    const FCGI_DATA = 8;
    const FCGI_GET_VALUES = 9;
    const FCGI_GET_VALUES_RESULT = 10;
    const FCGI_UNKNOWN_TYPE = 11;
    const FCGI_MAXTYPE = self::FCGI_UNKNOWN_TYPE;

    const FCGI_KEEP_CONN = 1;

    const FCGI_RESPONDER = 1;
    const FCGI_AUTHORIZER = 2;
    const FCGI_FILTER = 3;

    private $_socket;
    private $_stream;

    public function __construct() {

        /*
        $this->_socket = socket_create(AF_UNIX, SOCK_STREAM, 0);
        $ret = socket_connect($this->_socket, '/var/run/php/php7.2-fpm.sock');
        if ($ret === false) {
            $this->_log('open error.');
        }
        */
        
        $this->_socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        $ret = socket_connect($this->_socket, '127.0.0.1', 9000);
        if ($ret === false) {
            $this->_log('connect error.');
        }
        
    }

    //
    // 5. Application Record Types
    //

    //
    // 5.1 FCGI_BEGIN_REQUEST
    //
    public function send_begin_request($request_id, $keepalive) {

        $this->send_record_header($request_id, self::FCGI_BEGIN_REQUEST, 8);
        $this->_send(pack(
            'nCCCCCC',
            self::FCGI_RESPONDER,
            $keepalive ? self::FCGI_KEEP_CONN : 0,
            0, 0, 0, 0, 0 // padding = 5
        ));

    }

    //
    // 5.2 Name-Value Pair Stream: FCGI_PARAMS
    //
    public function send_params($request_id, $params) {

        $paramsLength = 0;
        $stream = '';
        foreach($params as $key => $value) {
            list($pl, $s) = $this->_send_param($request_id, $key, $value);
            $paramsLength += $pl;
            $stream .= $s;
        }

        $pad = $this->send_record_header($request_id, self::FCGI_PARAMS, $paramsLength);
        $this->_send($stream);

        // padding 
        $this->_send(str_repeat(chr(0), $pad));

        $this->flush();
    }


    //
    // 3. Protocol Basics
    //

    //
    // 3.3 Records (All data is carried in "records")
    //
    public function send_record_header($request_id, $recordType, $contentLength) {

        $intPaddingLength = $contentLength % 8;
        if($intPaddingLength != 0) {
            $intPaddingLength = (8 - $intPaddingLength);
        }
        $this->_send(pack(
            'CCnnCC',
            self::FCGI_VERSION_1,
            $recordType,
            $request_id,
            $contentLength,
            $intPaddingLength, // paddingLength
            0 // reserved
        ));

        return $intPaddingLength;
    }

    //
    // 3.4 Name-Value Pairs
    //
    public function _send_param($request_id, $name, $value) {

        $paramsLength = 0;
        $stream = '';
        if (empty($name) || empty($value)) {
            return [$paramsLength, $stream];
        }
    
        $nameLength = strlen($name);
        $valueLength = strlen($value);

        $paramsLength = $nameLength + $valueLength + 2;

        if($nameLength < 0x80){
            $stream .= pack('C', $nameLength);
        } else {
            $stream .= pack('N', $nameLength);
            $paramsLength += 3;
        }

        if($valueLength < 0x80){
            $stream .= pack('C', $valueLength);
        }else{
            $stream .= pack('N', $valueLength);
            $paramsLength += 3;
        }

        $stream .= $name;
        $stream .= $value;

        //$this->_send($stream);
        return [$paramsLength, $stream];
    }

    //
    // 5.3 Byte Streams: FCGI_STDIN
    //
    public function send_stdin($request_id, $body) {

        while( strlen($body) > 65535 ) {
            $part = substr($body, 0, 65535);
            $body = substr($body, 65535);
            $pad = $this->send_record_header($request_id, self::FCGI_STDIN, strlen($part));
            $this->_send($part);
            $this->_send(str_repeat(chr(0), $pad));
        }

        $pad = $this->send_record_header($request_id, self::FCGI_STDIN, strlen($body));
        $this->_send($body);
        $this->_send(str_repeat(chr(0), $pad));
        $this->flush();
        
    }

    //
    // 5.3 Byte Streams: FCGI_STDOUT, FCGI_STDERR, 5.5 FCGI_END_REQUEST
    //
    public function recv() {
        
        $stdout = '';
        $stderr = '';

        while(true){

            if (!$this->_check_version()) {
                break;
            }
            
            $header = $this->_read(7);
            $parsed = unpack('Crecord_type/nrequest_id/ncontent_length/Cpadding', $header);
            // $header[6] // reserved

            switch ($parsed['record_type']) {
                case self::FCGI_STDOUT:
                    $stdout .= $this->_read($parsed['content_length']);
                    $this->_read($parsed['padding']);
                break;

                case self::FCGI_STDERR:
                    $stderr .= $this->_read($parsed['content_length']);
                    $this->_read($parsed['padding']);
                break;

                case self::FCGI_END_REQUEST:
                    $buffer = $this->_read(5 + 3); // 3 : padding
                    $stat = unpack('Nstart_stat/Cend_satt', $buffer);
                    $start_stat = $stat['start_stat'];
                    $endStat = $stat['end_satt'];
                break;
                default:
                    $this->_log('recv record type error: ' . $parsed['record_type']);
                break;

            }
        }

        return [$stdout, $stderr];

    }

    private function _check_version() {

        $raw = $this->_read();
        if ($raw === '') {
            return false;
        }
        $version = ord($raw);
        if($version < 0) {
            return false;
        }
        if($version != self::FCGI_VERSION_1){
            $this->_log('recv record version error: ' . $version);
            return false;
        }
        return true;
    }

    private function _send($msg) {
        $this->_stream .= $msg;
    }

    public function flush() {
        
        $ret = socket_write($this->_socket, $this->_stream);
        if ($ret === false) {
            $this->_log('flush error!');
            $this->_log($this->_stream);
        }
        $this->_stream = '';
    }

    private function _read($lentgh = 1) {
        return socket_read($this->_socket, $lentgh);
    }

    public function close() {
        socket_close($this->_socket);
    }

    private function _log($msg) {
        echo $msg.PHP_EOL;
    }


    // --------------------------------------------------------
    // Request Send
    public function send($params, $body = '', $ignore = false) {

        $request_id = rand(1, 1 << 16);

        // B. Typical Protocol Message Flow
        // {FCGI_BEGIN_REQUEST,   1, {FCGI_RESPONDER, 0}}
        $this->send_begin_request($request_id, false);
        
        // {FCGI_PARAMS,          1, "..."}
        $params['CONTENT_LENGTH'] = strlen($body);
        $this->send_params($request_id, $params);
        
        if(count($params) > 0){
            // {FCGI_PARAMS,          1, ""}
            $params = [];
            $this->send_params($request_id, $params);
        }
        
        // {FCGI_STDIN,           1, "..."}
        $this->send_stdin($request_id, $body);
        
        if (!empty($body)) {
            // {FCGI_STDIN,           1, ""}
            $this->send_stdin($request_id, "");
        }

        if ($ignore) {
            $this->close();
            return ['', ''];
        }
        
        // {FCGI_STDOUT,      1, "Content-type: text/html\r\n\r\n\n ... "}
        // {FCGI_END_REQUEST, 1, {0, FCGI_REQUEST_COMPLETE}}
        $results = $this->recv();
        
        $this->close();

        return $results;
    }

}

