package FCGI;
use strict;
use warnings;
use Socket;

use constant {
    FCGI_VERSION_1 => 1,
    FCGI_BEGIN_REQUEST => 1,
    FCGI_ABORT_REQUEST => 2,
    FCGI_END_REQUEST => 3,
    FCGI_PARAMS => 4,
    FCGI_STDIN => 5,
    FCGI_STDOUT => 6,
    FCGI_STDERR => 7,
    FCGI_DATA => 8,
    FCGI_GET_VALUES => 9,
    FCGI_GET_VALUES_RESULT => 10,
    FCGI_UNKNOWN_TYPE => 11,
    FCGI_MAXTYPE => 11,
    FCGI_KEEP_CONN => 1,
    FCGI_RESPONDER => 1,
    FCGI_AUTHORIZER => 2,
    FCGI_FILTER => 3,
};

sub connect {
    my $sock;
    socket($sock, AF_INET, SOCK_STREAM, getprotobyname('tcp')) or die("Cannot create socket: $!");

    my $packed_remote_host = inet_aton('127.0.0.1') or die("Cannot pack: $!");
    # ホスト名とポート番号をパック
    my $sock_addr = sockaddr_in(9000, $packed_remote_host) or die("Cannot pack: $!");
    # ソケットを使って接続
    connect($sock, $sock_addr) or die("Cannot connect: $!");

    return $sock;
};

sub build_record_header {
    my ($class, $request_id, $record_type, $content_length) = @_;

    my $padding_length = $content_length % 8;
    if($padding_length != 0) {
        $padding_length = (8 - $padding_length);
    }
    pack(
        'CCnnCC',
        FCGI_VERSION_1,
        $record_type,
        $request_id,
        $content_length,
        $padding_length,
        0,
    );
};

# 5.1 FCGI_BEGIN_REQUEST
sub build_begin_request {
    my ($class, $request_id) = @_;

    my $buf = $class->build_record_header($request_id, FCGI_BEGIN_REQUEST, 8);
    my $role = FCGI_RESPONDER;
    my $flags = 0;

    $buf .= pack(
        'nCCCCCC',
        $role,
        $flags,
        0,0,0,0,0
    );
};

# 5.2 Name-Value Pair Stream: FCGI_PARAMS
sub build_params {
    my ($class, $request_id, %params)  = @_;

    my $params_length = 0;
    my $content = '';
    #while (my ($k, $v) = each %params) {
    foreach my $k ( keys %params ) {
        my $v = $params{$k};
        if (!$v) {
            next;
        }
        my $klen = length($k);
        my $vlen = length($v);
        $params_length += ($klen + $vlen + 2);

        if ($klen < 127) {
            $content .= pack('C', $klen); # C: An unsigned char (octet) value.
        } else {
            $klen = $klen | 0x80000000;
            $content .= pack('N', $klen); # N: An unsigned quad value.
            $params_length += 3;
        }

        if ($vlen < 127) {
            $content .= pack('C', $vlen);
        } else {
            $vlen = $vlen | 0x80000000;
            $content .= pack('N', $vlen);
            $params_length += 3;
        }
        $content .= $k;
        $content .= $v;
    }

    my $padding_length = $params_length % 8;
    if($padding_length != 0) {
        $padding_length = (8 - $padding_length);
    }
    my $buf = $class->build_record_header($request_id, FCGI_PARAMS, $params_length);
    $buf .= $content;

    for (my $i = 0; $i < $padding_length; $i++) {
        $buf .= pack('C', 0);
    }

    return $buf;
};

# 5.3 Byte Streams: FCGI_STDIN
sub build_stdin {
    my ($class, $request_id, $body)  = @_;

    my $buf = '';
    while( length($body) > 65535 ) {
        my $part = substr($body, 0, 65535);
        $body = substr($body, 65535);
        $buf .= $class->build_stdin_detail($request_id, $part);
    }
    $buf .= $class->build_stdin_detail($request_id, $body);
    return $buf;

};

sub build_stdin_detail {

    my ($class, $request_id, $part)  = @_;

    my $content_length = length($part);
    my $padding_length = $content_length % 8;
    if($padding_length != 0) {
        $padding_length = (8 - $padding_length);
    }

    my $buf = $class->build_record_header($request_id, FCGI_STDIN, $content_length);
    $buf .= $part;
    for (my $i = 0; $i < $padding_length; $i++) {
        $buf .= pack('C', 0);
    }

    return $buf;
};


sub send {
    my ($class, $params, $body) = @_;

    my $sock = $class->connect();
    my $request_id = int(rand(1 << 16))  + 1;
    my $stream = '';

    my %params_hash = %{$params};
    $params_hash{'CONTENT_LENGTH'} = length($body).''; # 文字列に変換

    $stream = $class->build_begin_request($request_id);
    $stream .= $class->build_params($request_id, %params_hash);
    if (0 < keys %params_hash) {
        $stream .= $class->build_params($request_id);
    }

    print $sock $stream;
    $stream = '';

    $stream .= $class->build_stdin($request_id, $body);
    $stream .= $class->build_stdin($request_id, '');
    print $sock $stream;
    close($sock);

}

1;
__END__
